# XileRetro Support
<div align="center">
![XileRetro](.resources/logo.png) 

### Important Links
[Website](http://xileretro.net) :: [Wiki](http://wiki.xileretro.net) :: [Discord](https://discord.gg/hp7CS6k) :: [Game Download](http://wiki.xileretro.net/index.php?title=Download) :: [Support and Reports Tracker](https://gitlab.com/xileretro/xileretro-support)
</div>

## Support and Player Reports

If you want to open a **support ticket** or **report a player** for XileRetro, you're in the right place!

Here you can [create a **new ticket**](https://gitlab.com/xileretro/xileretro-support/-/issues) and get the help you need and also help us catch people abusing the game!

## Bug and Suggestion Tracker

If you've got a **bug report** or **game suggestion** for XileRetro, you're also *still* in the right place~

You can browse the [**existing issues**](https://gitlab.com/xileretro/xileretro-support/-/issues) or [create a **new one**](https://gitlab.com/xileretro/xileretro-support/-/issues/new).

Please note that in order to post new ticket, you'll need to [sign in or sign up for a free GitLab account](https://gitlab.com/users/sign_in?redirect_to_referer=yes).

## Technical Support

If you're looking for quick **technical support** please use our [Discord Server](https://discord.gg/hp7CS6k) and someone should be able to help you.