## Report Information

### Reportee
<!-- insert after this line the name of the player you are reporting, then delete this line) -->

### Report Reason
<!-- insert after this line the reason for the report or the name of the infringed rule -->

### Report Details
<!-- insert after this line the content of your report, including any details that might be relevant to the case -->

### Attachments
<!-- insert after this line any evidence (screenshots, videos, replays, links) that might be relevant to the case -->

<!-- NOTE: tickets are public by default. If you wish to keep your report private, and only visible to you and the staff, make sure you enable the Confidential option on the right sidebar -->